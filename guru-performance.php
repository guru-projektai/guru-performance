<?php
/**
 * Plugin Name: Guru Performance
 * Description: Guru Performance
 * Version: 1.0.0
 * Author: Mindaugas Poškus
 */

if ( !defined( 'ABSPATH' ) ) {
    die;
}

define( 'Guru_Performance_FILE', __FILE__ );
define( 'Guru_Performance_PATH', plugin_dir_path( Guru_Performance_FILE ) );
define( 'Guru_Performance_URL', plugin_dir_url( Guru_Performance_FILE ) );

register_deactivation_hook( Guru_Performance_FILE, ['Guru_Performance', 'guru_performance_deactivate'] );

final class Guru_Performance
{
    /**
     * Plugin instance.
     *
     * @var Guru_Performance
     * @access private
     */
    private static $instance = null;

    /**
     * Get plugin instance.
     *
     * @return Guru_Performance
     * @static
     */
    public static function get_instance()
    {
        if ( ! isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Guru_Performance constructor.
     */
    private function __construct()
    {
        register_activation_hook(Guru_Performance_FILE, [$this , 'guru_performance_activate']);

        $this->guru_performance_includes();

        add_action('wp_enqueue_scripts', [$this, 'enqueue']);
    }

    /**
     * Enqueue Scripts and Styles
     */
    public function enqueue()
    {
        wp_enqueue_script('perfume', Guru_Performance_URL . '/assets/dist/perfume.min.js');
        wp_enqueue_script('performance', Guru_Performance_URL . '/assets/js/index.js', ['perfume']);
    }

    /**
     * Run when deactivate plugin.
     */
    public static function guru_performance_deactivate()
    {
        require_once Guru_Performance_PATH . 'includes/guru-performance-deactivator.php';
        Guru_Performance_Deactivator::deactivate();
    }

    /**
     * Run when activate plugin.
     */
    public function guru_performance_activate()
    {
        require_once Guru_Performance_PATH . 'includes/guru-performance-activator.php';
        Guru_Performance_Activator::activate();
    }

    /**
     * Loading plugin functions files
     */
    public function guru_performance_includes()
    {
        require_once Guru_Performance_PATH . 'includes/guru-performance-functions.php';
    }
}

function Guru_Performance()
{
    return Guru_Performance::get_instance();
}

$GLOBALS['Guru_Performance'] = Guru_Performance();
