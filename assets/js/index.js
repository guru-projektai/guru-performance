new Perfume({
  analyticsTracker: (options) => {
    const { metricName, data } = options

    switch (metricName) {
      case 'navigationTiming':
        if (data && data.timeToFirstByte) {
          sendToAnalytics('timeToFirstByte', data.timeToFirstByte)
        }
        break
      case 'fp':
        sendToAnalytics('firstPaint', data)
        break
      case 'fcp':
        sendToAnalytics('firstContentfulPaint', data)
        break
      case 'fid':
        sendToAnalytics('firstInputDelay', data)
        break
      case 'lcp':
        sendToAnalytics('largestContentfulPaint', data)
        break
      case 'cls':
        sendToAnalytics('cumulativeLayoutShift', data)
        break
    }
  },
  logging: true
});

function sendToAnalytics(metricName, value) {
  if (window.location.hostname === 'smartcasinoguide.com') {
    dataLayer.push({
      event: 'GuruPerformance',
      guruPerformanceType: metricName,
      guruPerformanceDuration: value,
    })
  }
  console.log(metricName, value)
}
